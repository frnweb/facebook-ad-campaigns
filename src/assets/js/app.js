import 'babel-polyfill'
import $ from 'jquery';
import whatInput from 'what-input'
import carousels from './lib/carousels'
//
import './dialogtech'
import googleAnalytics from './google-analytics'
import hotjar from './hotjar'
import liveHelpNow from './live-help-now'
import fbAssessmentEvent from './fb-assessment-event'

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

// Carousel
carousels()

// Live Help Now
liveHelpNow()

// Google Analytics
googleAnalytics()

//Facebook Assessment Click Event
fbAssessmentEvent()

