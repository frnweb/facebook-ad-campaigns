import $ from 'jquery'
import analytics from 'universal-ga'

const gaId = 'UA-5495921-1'

const dataAttrClickHandler = event => {
  const { gaCategory, gaAction, gaLabel } = $(event.currentTarget).data()

  analytics.event(gaCategory, gaAction, { eventLabel: gaLabel })
}

export default () => {
  $(document).ready(() => {
    analytics.initialize(gaId)
    analytics.pageview(window.location.pathname)

    $('[data-ga-event]').on('click', dataAttrClickHandler)
  })
}