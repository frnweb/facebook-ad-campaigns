import $ from 'jquery'
import 'slick-carousel'

export default () => {
  $(document).ready(() => {
    $('#locations-slider').slick({
      prevArrow: `<button class="slick-prev"></button>`,
      nextArrow: `<button class="slick-next"></button>`,
      dots: true,
      });
  });
};
